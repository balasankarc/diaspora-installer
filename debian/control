Source: diaspora-installer
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper (>= 9), gem2deb (>= 0.7.5~), po-debconf
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-ruby-extras/diaspora-installer.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-ruby-extras/diaspora-installer.git;a=summary
Homepage: http://wiki.debian.org/Diaspora
XS-Ruby-Versions: all

Package: diaspora-installer
Architecture: all
Section: contrib/ruby
XB-Ruby-Versions: ${ruby:Versions}
Pre-Depends: postgresql-client, dbconfig-common, adduser,
 diaspora-common (>= 0.5.0.1+debian2)
Depends: ${shlibs:Depends}, ${misc:Depends}, ruby (>= 2.0) | ruby-interpreter,
 wget,
 ruby-dev,
 libpq-dev,
 build-essential,
 libssl-dev,
 libcurl4-openssl-dev,
 libxml2-dev,
 libxslt-dev,
 imagemagick,
 ghostscript,
 libmagickwand-dev
Conflicts: diaspora, bundler (<< 1.9)
Replaces: diaspora, bundler (<< 1.9)
Description: distributed social networking service - installer
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users to set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about Diaspora at http://diasporafoundation.org
 .
 This dummy package downloads diaspora (also pulling in runtime
 dependencies as rubygems) and configures it to use PostgreSQL and
 Nginx.

Package: diaspora-common
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Pre-Depends: postgresql-client, dbconfig-common, adduser, bc
Depends: ${shlibs:Depends}, ${misc:Depends}, ruby | ruby-interpreter,
 nodejs,
 curl,
 postgresql,
 redis-server,
 sudo,
 ruby-rspec,
 rake,
 mta | exim4,
 net-tools,
 nginx
Conflicts: diaspora-installer (<< 0.3)
Breaks: diaspora-installer (<< 0.3)
Suggests: easy-rsa
Description: distributed social networking service - common files
 Diaspora (currently styled diaspora* and formerly styled DIASPORA*) is a free
 personal web server that implements a distributed social networking service.
 Installations of the software form nodes (termed "pods") which make up the
 distributed Diaspora social network.
 .
 Diaspora is intended to address privacy concerns related to centralized
 social networks by allowing users to set up their own server (or "pod") to
 host content; pods can then interact to share status updates, photographs,
 and other social data. It allows its users to host their data with a
 traditional web host, a cloud-based host, an ISP, or a friend. The framework,
 which is being built on Ruby on Rails, is free software and can be
 experimented with by external developers.
 .
 Learn more about Diaspora at http://diasporafoundation.org
 .
 It provides files common for the diaspora and diaspora-installer packages.

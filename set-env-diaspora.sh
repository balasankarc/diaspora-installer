#! /bin/sh

# Read configuration from file
. /etc/diaspora/diaspora-common.conf
echo "Using ${diaspora_conf}..."
if ! grep RAILS_ENV ${diaspora_conf}
	then echo export RAILS_ENV=production >> ${diaspora_conf}
fi

if ! grep DB ${diaspora_conf} 
		then echo export DB=postgres >> ${diaspora_conf}
fi
	
# source diaspora variables	
. ${diaspora_conf}


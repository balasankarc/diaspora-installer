#! /bin/sh
# Read configuration values	
. /etc/diaspora/diaspora-common.conf

echo "Initializing database..."
su diaspora -s /bin/sh -c 'bundle exec rake db:create db:schema:load'
echo "Precompiling assets..."
su diaspora -s /bin/sh -c 'bundle exec rake assets:precompile'
su diaspora -s /bin/sh -c 'touch public/source.tar.gz'
echo "Starting diaspora service..."
su diaspora -s /bin/sh -c 'nohup ./script/server &'

if grep https ${diaspora_conf}
then 
	mkdir -p ${diaspora_ssl_path}
	echo "Copy $SERVERNAME-bundle.pem and $SERVERNAME.key to /etc/diaspora/ssl"
	echo "And reload nginx, run # /etc/init.d/nginx reload"
fi

echo "To stop diaspora, run # /etc/init.d/diaspora stop"
echo "To see the service status, run # /etc/init.d/diaspora status"
echo "To start diaspora service, run # /etc/init.d/diaspora start"

echo "Visit your pod at $ENVIRONMENT_URL"

#!/bin/sh
if [ "x"$1 = "x" ]
then
	echo "usage: $0 <domain name>"
	exit 1
fi

# Read configuration values
. /etc/diaspora/diaspora-common.conf

nginx_conf_example=/usr/share/doc/diaspora-common/nginx.conf.example
 
if ! grep SERVERNAME ${diaspora_conf}
	then 
		echo export SERVERNAME=$1 >> ${diaspora_conf}
		export SERVERNAME=$1
		echo export ENVIRONMENT_URL=https://$SERVERNAME >> ${diaspora_conf}
		export ENVIRONMENT_URL=https://$SERVERNAME
		if test -f ${nginx_conf_example}
		then
			sed -e "s/SERVERNAME_FIXME/$1/"\
			 -e "s/DIASPORA_SSL_PATH_FIXME/\\/etc\\/diaspora/"\
			 -e "s/DIASPORA_ROOT_FIXME/\\/usr\\/share\\/diaspora/"\
			 ${nginx_conf_example} >/etc/nginx/sites-available/$1
			ln -fs /etc/nginx/sites-available/$1 /etc/nginx/sites-enabled/
		else
			echo "nginx.conf.example not found"
			exit 1
		fi
fi

